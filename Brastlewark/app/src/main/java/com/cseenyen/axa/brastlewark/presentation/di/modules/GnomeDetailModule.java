package com.cseenyen.axa.brastlewark.presentation.di.modules;

import com.cseenyen.axa.brastlewark.domain.executor.PostExecutionThread;
import com.cseenyen.axa.brastlewark.domain.executor.ThreadExecutor;
import com.cseenyen.axa.brastlewark.domain.interactor.GetGnomeDetailUseCase;
import com.cseenyen.axa.brastlewark.domain.interactor.GetGnomesUseCase;
import com.cseenyen.axa.brastlewark.domain.interactor.UseCase;
import com.cseenyen.axa.brastlewark.domain.repository.GnomeRepository;
import com.cseenyen.axa.brastlewark.presentation.di.ActivityScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by candicealtran on 16/03/16.
 */

@Module
public class GnomeDetailModule {
    private int gnomeId = -1;

    public GnomeDetailModule(int gnomeId) {
        this.gnomeId = gnomeId;
    }

    @Provides
    @ActivityScope
    @Named("")
    UseCase provideGetGnomeDetailUseCase(GetGnomeDetailUseCase getGnomeDetail) {
        return getGnomeDetail;
    }

    @Provides
    @ActivityScope
    @Named("GnomeDetailUseCase")
    UseCase provideGetUserDetailsUseCase(GnomeRepository gnomeRepository, ThreadExecutor threadExecutor,
                                         PostExecutionThread postExecutionThread) {
        return new GetGnomeDetailUseCase(this.gnomeId, gnomeRepository, threadExecutor, postExecutionThread);
    }
}
