package com.cseenyen.axa.brastlewark.data.repository;

import com.cseenyen.axa.brastlewark.data.model.Brastlewark;
import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.data.network.ApiService;
import com.cseenyen.axa.brastlewark.domain.repository.GnomeRepository;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by cseenyen on 27/02/16.
 */
@Singleton
public class GnomeRepositoryImpl implements GnomeRepository {

    @Inject
    public ApiService apiService;

    Gson gson;

    private Map<Integer, Gnome> brastlewarkData;

    @Inject
    public GnomeRepositoryImpl(Gson gson) {
        this.gson = gson;
    }


    @Override
    public Observable<List<Gnome>> getGnomes() {
        Observable<Brastlewark> main = this.apiService.getGnomes();
        return this.apiService.getGnomes()
                        .map(new Func1<Brastlewark, List<Gnome>>() {
                            @Override
                            public List<Gnome> call(Brastlewark brastlewark) {
                                saveDataAsMap(brastlewark);
                                return brastlewark.getGnomes();
                            }
                        })
                        .flatMap(new Func1<List<Gnome>, Observable<List<Gnome>>>() {
                            @Override
                            public Observable<List<Gnome>> call(List<Gnome> gnomes) {
                                return Observable.just(gnomes);
                            }
                        });
    }

    @Override
    public Observable<Gnome> getGnomeDetail(int id) {
        return Observable.just(brastlewarkData.get(id));
    }

    private void saveDataAsMap(Brastlewark brastlewark) {
        brastlewarkData = new HashMap<Integer, Gnome>();
        List<Gnome> gnomes = brastlewark.getGnomes();
        for(Gnome gnome : gnomes) {
            brastlewarkData.put(gnome.getId(), gnome);
        }
    }

}
