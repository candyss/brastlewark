package com.cseenyen.axa.brastlewark.presentation.di.modules;

import com.cseenyen.axa.brastlewark.domain.executor.PostExecutionThread;
import com.cseenyen.axa.brastlewark.domain.executor.ThreadExecutor;
import com.cseenyen.axa.brastlewark.domain.interactor.GetGnomesUseCase;
import com.cseenyen.axa.brastlewark.domain.interactor.UseCase;
import com.cseenyen.axa.brastlewark.presentation.di.ActivityScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cseenyen on 12/03/16.
 */

@Module
public class GnomeModule {

    @Provides
    @ActivityScope
    @Named("GnomeListUseCase")
    UseCase provideGetGnomeListUseCase(GetGnomesUseCase getUserList) {
        return getUserList;
    }
}
