package com.cseenyen.axa.brastlewark.data;

/**
 * Created by cseenyen on 06/03/16.
 */
public class Constants {

    public static class SharedPreferencesConstants {
        public static final String BASE_KEY = SharedPreferencesConstants.class.getSimpleName();
        public static final String KEY_SESSION = BASE_KEY + "_session";
        public static final String KEY_LAUNCH_TIME = BASE_KEY + "_launchTime";
    }
}
