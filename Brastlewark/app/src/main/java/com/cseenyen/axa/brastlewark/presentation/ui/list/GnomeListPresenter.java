package com.cseenyen.axa.brastlewark.presentation.ui.list;

import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.domain.interactor.DefaultSubscriber;
import com.cseenyen.axa.brastlewark.domain.interactor.UseCase;
import com.cseenyen.axa.brastlewark.presentation.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GnomeListPresenter implements BasePresenter {

    private GnomeListView view;
    private UseCase getGnomeListUseCase;

    @Inject
    public GnomeListPresenter(@Named("GnomeListUseCase") UseCase getGnomeListUseCase) {
        this.getGnomeListUseCase = getGnomeListUseCase;
    }

    public void setView(GnomeListView view) {
        this.view = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    private void hideLoadingView() {
        view.hideLoading();
    }

    private void showLoadingView() {
        view.showLoading();
    }

    private void hideRetryView() {
        view.hideRetry();
    }

    private void showRetryView() {
        view.showRetry();
    }

    private void displayErrorMessage(String message) {
        hideLoadingView();
        showRetryView();
        view.displayErrorMessage(message);
    }

    @Override
    public void destroy() {
        view = null;
        getGnomeListUseCase.unsubscribe();
    }

    public void getGnomes() {
        showLoadingView();
        hideRetryView();
        this.getGnomeListUseCase.execute(new GetGnomesSubscriber());
    }

    private void renderGnomesList(List<Gnome> result) {
        hideLoadingView();
        view.renderGnomeList(result);
    }

    private final class GetGnomesSubscriber extends DefaultSubscriber<List<Gnome>> {

        @Override
        public void onCompleted() {
            hideLoadingView();
        }

        @Override
        public void onError(Throwable e) {
            displayErrorMessage(e.getMessage());
        }

        @Override
        public void onNext(List<Gnome> result) {
            renderGnomesList(result);
        }
    }
}
