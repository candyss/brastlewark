package com.cseenyen.twittertime.presentation.tweet;

import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.domain.interactor.GetGnomesUseCase;
import com.cseenyen.axa.brastlewark.presentation.ui.list.GnomeListView;
import com.cseenyen.twittertime.R;
import com.cseenyen.twittertime.domain.interactor.SaveSessionUseCase;
import com.cseenyen.twittertime.domain.interactor.SaveUpdatedTimeUseCase;
import com.cseenyen.twittertime.domain.interactor.SearchUseCase;
import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.models.Tweet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Created by cseenyen on 07/03/16.
 */
public class GnomeListPresenter {

    private GnomeListPresenter presenter;

    @Mock
    GnomeListView listView;
    @Mock
    GetGnomesUseCase getGnomesUseCase;
    @Mock
    List<Gnome> gnomes;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        GnomeListPresenter = new TweetPresenter(getGnomesUseCase);
        presenter.setView(listView);
    }

    @Test
    public void onDisplayError_viewDisplayError() {
        presenter.displayError("An error has occurred");

        verify(listView).hideLoading();
        verify(listView).displayError("An error has occurred");
    }

    @Test
    public void onListReceived_displayList() {
        presenter.renderGnomesList(gnomes);

        verify(listView).hideLoading();
        verify(listView).renderGnomeList(gnomes);
    }
}
