package com.cseenyen.axa.brastlewark.presentation.ui.image;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.cseenyen.axa.brastlewark.BrastlewarkApplication;
import com.cseenyen.axa.brastlewark.R;
import com.cseenyen.axa.brastlewark.data.model.Brastlewark;
import com.cseenyen.axa.brastlewark.presentation.base.BaseActivity;
import com.cseenyen.axa.brastlewark.presentation.di.modules.FullScreenImageModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeDetailModule;
import com.cseenyen.axa.brastlewark.presentation.ui.detail.GnomeDetailFragment;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by candicealtran on 17/03/16.
 */
public class FullScreenImageActivity extends BaseActivity implements FullScreenImageView {

    private static final String INTENT_EXTRA_PARAM_IMAGE_URL = "com.cseenyen.axa.brastlewark.INTENT_EXTRA_PARAM_IMAGE_URL";
    private static final String INSTANCE_STATE_PARAM_IMAGE_URL = "com.cseenyen.axa.brastlewark.image.INSTANCE_STATE_PARAM_IMAGE_URL";

    private String imageUrl;

    @Inject
    FullScreenImagePresenter presenter;
    @Inject
    Picasso picasso;

    @Bind(R.id.fullscreen_image_layout)
    View layout;
    @Bind(R.id.fullscreen_image)
    ImageView fullScreenImage;

    public static Intent getCallingIntent(Context context, String imageUrl) {
        Intent callingIntent = new Intent(context, FullScreenImageActivity.class);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_IMAGE_URL, imageUrl);
        return callingIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen_image_layout);
        ButterKnife.bind(this);

        initializeActivity(savedInstanceState);
        setupActivityComponent();
        presenter.setView(this);
        presenter.initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        presenter.destroy();
        BrastlewarkApplication.get(this).releaseFullScreenImageComponent();
    }

    private void initializeActivity(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            this.imageUrl = getIntent().getStringExtra(INTENT_EXTRA_PARAM_IMAGE_URL);
        } else {
            this.imageUrl = savedInstanceState.getString(INSTANCE_STATE_PARAM_IMAGE_URL);
        }
    }

    protected void setupActivityComponent() {
        BrastlewarkApplication.get(this)
                .createFullScreenImageComponent(new FullScreenImageModule(this.imageUrl))
                .inject(this);
    }

    @Override
    public void renderImage() {
        picasso.load(this.imageUrl)
                .error(R.drawable.image_error)
                .into(this.fullScreenImage);
    }

    @OnClick(R.id.fullscreen_image_layout)
    public void onLayoutClicked() {
        this.finish();
    }
}
