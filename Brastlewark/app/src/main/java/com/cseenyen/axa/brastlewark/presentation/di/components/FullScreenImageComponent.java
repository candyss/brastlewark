package com.cseenyen.axa.brastlewark.presentation.di.components;

import com.cseenyen.axa.brastlewark.presentation.di.ActivityScope;
import com.cseenyen.axa.brastlewark.presentation.di.modules.FullScreenImageModule;
import com.cseenyen.axa.brastlewark.presentation.ui.image.FullScreenImageActivity;

import dagger.Subcomponent;

/**
 * Created by candicealtran on 17/03/16.
 */

@ActivityScope
@Subcomponent(
        modules = {
                FullScreenImageModule.class
        }
)

public interface FullScreenImageComponent {

    void inject(FullScreenImageActivity activity);

}
