package com.cseenyen.axa.brastlewark.presentation.ui.image;

import android.support.annotation.NonNull;

import com.cseenyen.axa.brastlewark.presentation.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by candicealtran on 17/03/16.
 */
public class FullScreenImagePresenter implements BasePresenter {

    private FullScreenImageView view;

    @Inject
    public FullScreenImagePresenter() {

    }

    public void setView(@NonNull FullScreenImageView view) {
        this.view = view;
    }

    public void initialize() {
        this.view.renderImage();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        view = null;
    }
}
