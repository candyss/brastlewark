package com.cseenyen.axa.brastlewark.domain.interactor;

import com.cseenyen.axa.brastlewark.domain.executor.PostExecutionThread;
import com.cseenyen.axa.brastlewark.domain.executor.ThreadExecutor;
import com.cseenyen.axa.brastlewark.domain.repository.GnomeRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GetGnomeDetailUseCase extends UseCase {

    private GnomeRepository gnomeRepository;
    private int gnomeId;

    @Inject
    public GetGnomeDetailUseCase(int id, GnomeRepository gnomeRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.gnomeRepository = gnomeRepository;
        this.gnomeId = id;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.gnomeRepository.getGnomeDetail(this.gnomeId);
    }
}
