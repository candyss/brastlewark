package com.cseenyen.axa.brastlewark.presentation.ui.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.cseenyen.axa.brastlewark.R;
import com.cseenyen.axa.brastlewark.BrastlewarkApplication;
import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.presentation.base.BaseActivity;
import com.cseenyen.axa.brastlewark.presentation.di.components.ApplicationComponent;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeModule;
import com.cseenyen.axa.brastlewark.presentation.ui.detail.GnomeDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GnomeListActivity extends BaseActivity implements GnomeListView, GnomeListAdapter.OnItemClickListener,
        SearchView.OnQueryTextListener {

    @Inject
    GnomeListPresenter gnomeListPresenter;
    @Inject
    GnomeListAdapter gnomeListAdapter;
    @Inject
    GridAutofitLayoutManager layoutManager;

    private List<Gnome> gnomes;

    @Bind(R.id.gnomes_list)
    RecyclerView gnomesListView;
    @Bind(R.id.progress_rel_layout)
    RelativeLayout progressLayout;
    @Bind(R.id.retry_rel_layout)
    RelativeLayout retryLayout;
    @Bind(R.id.retry_button)
    Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gnome_list_layout);
        setupActivityComponent();

        ButterKnife.bind(this);

        gnomeListPresenter.setView(this);
        setupRecyclerView();
        gnomeListPresenter.getGnomes();

    }

    protected void setupActivityComponent() {
        BrastlewarkApplication.get(this)
                .getApplicationComponent()
                .plus(new GnomeModule())
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gnomeListPresenter.resume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
        }

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gnomeListPresenter.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        gnomeListPresenter.destroy();
    }

    @Override
    public Context context() {
        return getApplicationContext();
    }

    @Override
    public void showLoading() {
        this.progressLayout.setVisibility(View.VISIBLE);
        setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        this.progressLayout.setVisibility(View.GONE);
        setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
        this.retryLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.retryLayout.setVisibility(View.GONE);
    }

    @Override
    public void displayErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.retry_button)
    public void onRetryClicked() {
        gnomeListPresenter.getGnomes();
    }

    private void setupRecyclerView() {
        this.gnomeListAdapter.setOnItemClickListener(this);

        this.gnomesListView.setHasFixedSize(true); // allows optimizations knowing that the size of the adapter content will not change
        this.gnomesListView.setLayoutManager(layoutManager);
        this.gnomesListView.setAdapter(gnomeListAdapter);
    }

    @Override
    public void onUserItemClicked(Gnome gnome) {
        Intent intentToLaunch = GnomeDetailActivity.getCallingIntent(this, gnome.getId());
        startActivity(intentToLaunch);
    }

    @Override
    public void renderGnomeList(List<Gnome> gnomesList) {
        this.gnomes = gnomesList;
        if (this.gnomes != null) {
            this.gnomeListAdapter.setUsersCollection(gnomesList);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        gnomeListAdapter.getFilter().filter(query);
        return true;
    }
}
