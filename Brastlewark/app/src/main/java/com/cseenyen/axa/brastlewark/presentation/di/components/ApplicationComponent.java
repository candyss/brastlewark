/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cseenyen.axa.brastlewark.presentation.di.components;

import android.content.Context;
import android.content.SharedPreferences;

import com.cseenyen.axa.brastlewark.data.DataModule;
import com.cseenyen.axa.brastlewark.data.network.ApiService;
import com.cseenyen.axa.brastlewark.domain.executor.PostExecutionThread;
import com.cseenyen.axa.brastlewark.domain.executor.ThreadExecutor;
import com.cseenyen.axa.brastlewark.presentation.base.BaseActivity;
import com.cseenyen.axa.brastlewark.presentation.di.modules.ApplicationModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.FullScreenImageModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeDetailModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    ApiService apiService();

    SharedPreferences sharedPreferences();

    // Subcomponents
    GnomeComponent plus(GnomeModule module);

    GnomeDetailComponent plus(GnomeDetailModule module);

    FullScreenImageComponent plus(FullScreenImageModule module);
}
