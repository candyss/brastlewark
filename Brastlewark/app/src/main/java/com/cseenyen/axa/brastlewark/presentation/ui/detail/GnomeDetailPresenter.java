package com.cseenyen.axa.brastlewark.presentation.ui.detail;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.domain.interactor.DefaultSubscriber;
import com.cseenyen.axa.brastlewark.domain.interactor.UseCase;
import com.cseenyen.axa.brastlewark.presentation.base.BasePresenter;
import com.cseenyen.axa.brastlewark.presentation.ui.list.GnomeListPresenter;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GnomeDetailPresenter implements BasePresenter {

    private GnomeDetailView view;
    private UseCase getGnomeDetailsUseCase;
    private Gnome gnome;

    @Inject
    public GnomeDetailPresenter(@Named("GnomeDetailUseCase") UseCase getUserDetailsUseCase) {
        this.getGnomeDetailsUseCase = getUserDetailsUseCase;
    }

    public void setView(@NonNull GnomeDetailView view) {
        this.view = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getGnomeDetailsUseCase.unsubscribe();
        this.view = null;
    }

    private void displayErrorMessage(String message) {
        view.displayErrorMessage(message);
    }

    public void initialize() {
        this.getGnomeDetailsUseCase.execute(new GnomeDetailSubscriber());
    }

    private void renderGnomeDetail(Gnome gnome) {
        view.renderGnome(gnome);
    }

    public void onImageClicked() {
        if(isImageUrlValid()) {
            view.displayGnomeImageFullScreen(gnome.getThumbnail());
        }
    }

    private boolean isImageUrlValid() {
        return this.gnome != null && !TextUtils.isEmpty(this.gnome.getThumbnail());
    }

    private final class GnomeDetailSubscriber extends DefaultSubscriber<Gnome> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            displayErrorMessage(e.getMessage());
        }

        @Override
        public void onNext(Gnome gnome) {
            GnomeDetailPresenter.this.gnome = gnome;
            renderGnomeDetail(gnome);
        }
    }
}
