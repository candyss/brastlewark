package com.cseenyen.axa.brastlewark.presentation.di.modules;

import dagger.Module;

/**
 * Created by candicealtran on 17/03/16.
 */

@Module
public class FullScreenImageModule {

    private String imageUrl = "";

    public FullScreenImageModule(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
