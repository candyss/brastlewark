package com.cseenyen.axa.brastlewark.data;

import android.app.Application;
import android.net.Uri;
import android.util.Log;

import com.cseenyen.axa.brastlewark.data.network.ApiModule;
import com.cseenyen.axa.brastlewark.data.repository.GnomeRepositoryImpl;
import com.cseenyen.axa.brastlewark.domain.repository.GnomeRepository;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by cseenyen on 06/03/16.
 */

@Module(
        includes = ApiModule.class
)
public class DataModule {
    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        return new OkHttpClient();
    }

    static OkHttpClient.Builder createOkHttpClient(Application app) {
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        return new OkHttpClient.Builder().cache(cache);
    }

    @Provides
    @Singleton
    GnomeRepository provideLoginRepository(GnomeRepositoryImpl gnomeRepository) {
        return gnomeRepository;
    }
}
