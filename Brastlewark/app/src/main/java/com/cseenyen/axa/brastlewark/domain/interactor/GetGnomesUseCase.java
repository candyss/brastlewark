package com.cseenyen.axa.brastlewark.domain.interactor;

import com.cseenyen.axa.brastlewark.domain.executor.PostExecutionThread;
import com.cseenyen.axa.brastlewark.domain.executor.ThreadExecutor;
import com.cseenyen.axa.brastlewark.domain.repository.GnomeRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GetGnomesUseCase extends UseCase {

    private GnomeRepository gnomeRepository;

    @Inject
    protected GetGnomesUseCase(GnomeRepository gnomeRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.gnomeRepository = gnomeRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.gnomeRepository.getGnomes();
    }
}
