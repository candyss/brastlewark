package com.cseenyen.axa.brastlewark.presentation.ui.detail;

import com.cseenyen.axa.brastlewark.data.model.Gnome;

import java.util.List;

/**
 * Created by candicealtran on 16/03/16.
 */
public interface GnomeDetailView {
    /**
     * Displays an error message
     *
     * @param message String to display
     */
    void displayErrorMessage(String message);

    /**
     * Render the detailed information of a {@link Gnome}
     *
     * @param gnome
     *            The {@link Gnome} that will be rendered
     */
    void renderGnome(Gnome gnome);

    /**
     * Display the gnome's image in full screen
     *
     * @param thumbnail
     *          the url of the image
     */
    void displayGnomeImageFullScreen(String thumbnail);
}
