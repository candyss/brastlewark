package com.cseenyen.axa.brastlewark.presentation.ui.detail;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.cseenyen.axa.brastlewark.R;
import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.presentation.base.BaseFragment;
import com.cseenyen.axa.brastlewark.presentation.di.components.GnomeDetailComponent;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GnomeDetailFragment extends BaseFragment implements GnomeDetailView {

    public interface onDisplayImageFullScreenListener {
        public void displayImageFullScreen(String url);
    }

    @Inject
    GnomeDetailPresenter presenter;
    @Inject
    Picasso picasso;

    @Bind(R.id.big_image)
    ImageView gnomeImage;
    @Bind(R.id.detail_name_content)
    TextView gnomeName;
    @Bind(R.id.detail_age_content)
    TextView gnomeAge;
    @Bind(R.id.detail_weight_content)
    TextView gnomeWeight;
    @Bind(R.id.detail_height_content)
    TextView gnomeHeight;
    @Bind(R.id.detail_hair_color_content)
    TextView gnomeHairColor;
    @Bind(R.id.detail_professions_content)
    TextView gnomeProfessions;
    @Bind(R.id.detail_friends_content)
    TextView gnomeFriends;

    private onDisplayImageFullScreenListener imageListener;

    public GnomeDetailFragment() {
        setRetainInstance(true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity act = (Activity) context;
        if (act instanceof onDisplayImageFullScreenListener) {
            this.imageListener = (onDisplayImageFullScreenListener) act;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(GnomeDetailComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.gnome_detail_layout, container, false);
        ButterKnife.bind(this, fragmentView);

        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.presenter.setView(this);
        loadGnomeDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.presenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.presenter.pause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.presenter.destroy();
    }

    @Override
    public void renderGnome(Gnome gnome) {
        if (gnome != null) {
            picasso.load(gnome.getThumbnail())
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_error)
                    .fit()
                    .centerCrop()
                    .into(gnomeImage);
            gnomeName.setText(gnome.getName());
            gnomeAge.setText(String.valueOf(gnome.getAge()));
            gnomeWeight.setText(String.valueOf(gnome.getWeight()));
            gnomeHeight.setText(String.valueOf(gnome.getHeight()));
            gnomeHairColor.setText(gnome.getHairColor());
            gnomeProfessions.setText(gnome.getProfessionsForDisplay());
            gnomeFriends.setText(gnome.getFriendsForDisplay());
        }
    }

    @Override
    public void displayGnomeImageFullScreen(String thumbnail) {
        this.imageListener.displayImageFullScreen(thumbnail);
    }

    public void displayErrorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void loadGnomeDetail() {
        if (this.presenter != null) {
            this.presenter.initialize();
        }
    }

    @OnClick(R.id.big_image)
    public void onImageClicked() {
        presenter.onImageClicked();
    }
}
