package com.cseenyen.axa.brastlewark.domain.repository;

import com.cseenyen.axa.brastlewark.data.model.Brastlewark;
import com.cseenyen.axa.brastlewark.data.model.Gnome;

import java.util.List;

import rx.Observable;

/**
 * Created by cseenyen on 27/02/16.
 */
public interface GnomeRepository {
    /**
     * Get an {@link Observable} which will emit a list of the {@link Gnome}
     */
    Observable<List<Gnome>> getGnomes();

    /**
     * Get a {@link Gnome} with the specified id
     */
    Observable<Gnome> getGnomeDetail(int id);
}
